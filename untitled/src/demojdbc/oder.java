package demojdbc;

public class oder {
    int oderNumber;
    String oderDate;
    String requiredDate;

    public oder() {
    }

    public oder(int oderNumber, String oderDate, String requiredDate) {
        this.oderNumber = oderNumber;
        this.oderDate = oderDate;
        this.requiredDate = requiredDate;
    }

    public int getOderNumber() {
        return oderNumber;
    }

    public void setOderNumber(int oderNumber) {
        this.oderNumber = oderNumber;
    }

    public String getOderDate() {
        return oderDate;
    }

    public void setOderDate(String oderDate) {
        this.oderDate = oderDate;
    }

    public String getRequiredDate() {
        return requiredDate;
    }

    public void setRequiredDate(String requiredDate) {
        this.requiredDate = requiredDate;
    }

    @Override
    public String toString() {
        return "oder{" +
                "oderNumber='" + oderNumber + '\'' +
                ", oderDate='" + oderDate + '\'' +
                ", requiredDate='" + requiredDate + '\'' +
                '}';
    }
}
