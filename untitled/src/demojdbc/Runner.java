package demojdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Runner {
    public static void main(String[] args) throws SQLException {
        List<sutdent> student = new ArrayList<>();
        Connection connection = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/one",
                "root", "123456"
        );
        System.out.println(connection.isClosed());
//Buoc 1 tao url
//b2 them user password
//b3 Tao ket noi
//b4
        try (PreparedStatement statement = connection.prepareStatement("""
            SELECT *
            FROM student
        """)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
//                String val1 = resultSet.getString(1); // by column index
                String name = resultSet.getString("name"); // by column name
                int class_id = resultSet.getInt("class_id");
                student.add(new sutdent(name,class_id));
                // ... use val1 and val2 ...
//                System.out.println(val1);
//                System.out.println(name);
//                System.out.println(class_id);
            }
            System.out.println(student);
            resultSet.close();
        }finally {
            connection.close();
        }
    }
}
