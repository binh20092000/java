package demojdbc;

import java.sql.*;

public class bai3 {
    public static void main(String[] args) throws SQLException {
        Connection connection = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/classicmodels",
                "root", "123456"
        );
        try (
                PreparedStatement statement = connection.prepareStatement("""
           SELECT COUNT(*) AS soluongEmployees 
           FROM employees;
        """)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                int numberEmployees = resultSet.getInt("soluongEmployees"); // by column name


                System.out.println(numberEmployees);
            }

            resultSet.close();
        }finally {
            connection.close();
        }
    }
}
