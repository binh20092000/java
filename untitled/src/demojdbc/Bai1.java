package demojdbc;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Bai1 {
    public static void main(String[] args) throws SQLException {
        List<oder> oderList = new ArrayList<>();
        Connection connection = DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/classicmodels",
                "root", "123456"
        );
        try (
                PreparedStatement statement = connection.prepareStatement("""
           SELECT * 
           FROM orders LIMIT 5
        """)) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {

                int numberoder = resultSet.getInt("ordernumber"); // by column name
                String oderdate = resultSet.getString("orderdate");
                String requireddate = resultSet.getString("requireddate");
                oderList.add(new oder(numberoder,oderdate,requireddate));
            }
            for(int i = 0;i<oderList.size();i++){
                System.out.println(oderList.get(i).getOderDate() + " " +  oderList.get(i).getRequiredDate() + " " + oderList.get(i).getOderNumber());
            }
            resultSet.close();
        }finally {
            connection.close();
        }
    }

}


